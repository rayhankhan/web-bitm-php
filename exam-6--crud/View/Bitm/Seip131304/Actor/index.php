<?php
include_once "../../../../Src/Bitm/Seip131304/Actor/Actor.php";
?>


<?php

$obj = new Actor();
$Alldata = $obj->index();

if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>
<html>
<head>
    <title>all data</title>
</head>
<body align="middle">
<table border="1" align="middle">
    <tr >
        <th>SL</th>
        <th>Name</th>
        <th>Hobby</th>
        <th colspan="3">Action</th>
    </tr>
    <?php
    $serial = 1;
    if (isset($Alldata) && !empty($Alldata)) {

        foreach ($Alldata as $Singledata) {
            ?>

            <tr>
                <td><?php echo $serial++ ?></td>
                <td><?php echo $Singledata['name'] ?></td>
                <td><?php echo $Singledata['hobby'] ?></td>
                <td><a href="show.php?id=<?php echo $Singledata['id'] ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $Singledata['id'] ?>">Edit</a></td>
                <td><a href="delete.php?id=<?php echo $Singledata['id'] ?>">X</a></td>

            </tr>
        <?php }
    } else {
        ?>
        <tr>
            <td colspan="3">
                No available data
            </td>
        </tr>
    <?php } ?>
</table>
</body>
</html>
<a href="../../../../index.php">Back to Project</a>
<a href="create.php">Form</a>