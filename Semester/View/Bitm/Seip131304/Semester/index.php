<?php
include_once "../../../../Src/Bitm/Seip131304/Semester/Semester.php";
?>
<a href="../../../../index.php">Back to Project</a>
<a href="create.php">Form</a>

<?php
$obj = new Semester();
$Alldata = $obj->index();

if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>
<html>
<head>
    <title>all data</title>
</head>

<body>
<table border="1" align="center">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Semester</th>
        <th>Offer</th>
        <th>Cost</th>
        <th>Weber</th>
        <th>Total</th>
        <th colspan="8">Action</th>
    </tr>
    <?php
    $serial = 1;
    if (isset($Alldata) && !empty($Alldata)) {

        foreach ($Alldata as $Singledata) {
            ?>

            <tr>
                <td><?php echo $serial++ ?></td>
                <td><?php echo $Singledata['name'] ?></td>
                <td><?php echo $Singledata['semester'] ?></td>
                <td><?php echo $Singledata['offer'] ?></td>
                <td><?php echo $Singledata['cost'] ?></td>
                <td><?php echo $Singledata['weber'] ?></td>
                <td><?php echo $Singledata['total'] ?></td>
                
                <td><a href="show.php?id=<?php echo $Singledata['unique_id'] ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $Singledata['unique_id'] ?>">Edit</a></td>
                <td><a href="delete.php?id=<?php echo $Singledata['unique_id'] ?>">Delete</a></td>

            </tr>
        <?php }
    } else {
        ?>
        <tr>
            <td colspan="4" align="center">
                No available data
            </td>
        </tr>
    <?php } ?>
</table>
</body>
</html>
