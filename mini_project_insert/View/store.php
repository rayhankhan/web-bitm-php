<?php
include_once "../Src/Users.php";
$obj = new Users();
$obj->prepare($_POST);
$obj->store();

//echo "<pre>";
//print_r($_POST);

if (empty($_POST['username']) || empty($_POST['password']) || empty($_POST['cPassword']) || empty($_POST['email'])) {
    $_SESSION['Msg'] = "Fields are empty";
    header("location:signup.php");
} 
elseif ((strlen($_POST['username']) < 5) || (strlen($_POST['username']) > 10)) {
    $_SESSION['Msg'] = "username not valid";
    header("location:signup.php");
} elseif ((strlen($_POST['password']) < 6) || (strlen($_POST['password']) > 12)) {
    $_SESSION['Msg'] = "password not valid";
    header("location:signup.php");
} elseif ($_POST['password'] != $_POST['cPassword']) {
    $_SESSION['Msg'] = "password not Matched";
    header("location:signup.php");
}
else{
    if($obj->checkingUser() == 1){
            $_SESSION['Msg'] = "Username Exists";
            header("location:signup.php");
    }
 elseif ($obj->checkingEmail() == 1) {
            $_SESSION['Msg'] = "Email Exists";
            header("location:signup.php");
    }
 else {
        $obj->store();
    }
}