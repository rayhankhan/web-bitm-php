<?php

//for loop
for($x = 0; $x<10; $x++){
	echo "The number is : $x <br/>";
} echo "<br/>";

//foreach

$colors = array("red","green","blue","black");

foreach ($colors as $key => $value) {
	echo "$value <br/>";
} echo "<br/>";

//while

$x = 1;

while ($x < 10) {
	echo "The number is :$x <br/>";
	$x++;
} echo "<br/>";

//do while
$x = 2;

do {
	echo "The number is : $x <br/>";
	$x++;
} while ( $x > 5);
echo "<br/>";

//switch

$favcolor = "black";

switch ($favcolor) {
    case "red":
        echo "Your favorite color is red!";
        break;
    case "blue":
        echo "Your favorite color is blue!";
        break;
    case "green":
        echo "Your favorite color is green!";
        break;
    default:
        echo "Your favorite color is neither red, blue, nor green!";
}echo "<br/>";

//array_rand

$a=array("lamborgini","bmw","tesla","dodge","ferrari","mercedes-Benz","audi");
$random_keys=array_rand($a,3);
echo $a[$random_keys[0]] . "<br/>";
echo $a[$random_keys[1]] . "<br/>";
echo $a[$random_keys[2]] . "<br/>";


//array_replace

$a1 = array("blue","green");
$a2 = array("dodge","audi");

print_r(array_replace($a1, $a2));
echo "<br/>";

//array_recursive

$a1 = array("a"=>array("red"),"b"=>array("green","black"));
$a2 = array("a"=>array("dodge"),"b"=>array("audi"));

print_r(array_replace_recursive($a1, $a2));
echo "<br/>";

//array_reverse

$a=array("a"=>"Volvo","b"=>"BMW","c"=>"Toyota");
print_r(array_reverse($a));

echo "<br/>";

//array_search

$a=array("a"=>"audi","b"=>"dodge","c"=>"farrari");
echo array_search("audi",$a);

echo "<br/>";

//array_shift

$a=array("a"=>"red","b"=>"green","c"=>"blue");
echo array_shift($a)."<br/>";
print_r ($a);

echo "<br/>";

//array_sum

$a = array(5,10,25,30);
echo "array total is ".(array_sum($a));

echo "<br/>";

//array_unique

$a=array("a"=>"red","b"=>"green","c"=>"red");
print_r(array_unique($a));
echo "<br/>";

//array_unshift

$a=array("a"=>"dodge","b"=>"lamborgini");
array_unshift($a,"mercedes-Benz");
print_r($a);
echo "<br/>";

//arrray_walk
function myfunction($value,$key)
{
echo "The key $key has the value $value<br>";
}
$a=array("a"=>"lamborgini","b"=>"audi","c"=>"mercedes-Benz");
array_walk($a,"myfunction");
echo "<br/>";

//array_values

$a=array("Name"=>"Peter","Age"=>"41","Country"=>"USA");
print_r(array_values($a));
echo "<br/>";

//asort
$age=array("Peter"=>"35","Ben"=>"37","Joe"=>"43");
asort($age);

foreach($age as $x=>$x_value)
   {
   echo "Key=" . $x . ", Value=" . $x_value;
   echo "<br>";
   }
echo "<br/>";

//compact

$firstname = "Peter";
$firstcar = "lamborgini";
$age = "41";

$result = compact("firstname", "firstcar", "age");

print_r($result);
echo "<br/>";

//count

$cars=array("Volvo","BMW","Toyota");
echo count($cars);
echo "<br/>";

//current

$people = array("Peter", "Rayhan", "Glenn", "Cleveland");

echo current($people) . "<br>";
echo "<br/>";

//each

$people = array("Peter", "Joe", "Glenn", "Cleveland");
print_r (each($people));

echo "<br/>";

//in_array

$cars = array("dodge", "lamborgini", "ferrari", "mercedes-Benz");

if (in_array("mercedes-Benz", $cars))
  {
  echo "Match found";
  }
else
  {
  echo "Match not found";
  }
echo "<br/>";

//key

$people=array("Peter","Joe","Glenn","Cleveland");
echo "The key from the current position is: " . key($people);
echo "<br/>";

//list

$my_car = array("dodge","audi","tesla");

list($a, $b, $c) = $my_car;
echo "I have several cars, a $a, a $b and a $c.";
echo "<br/>";

//next

$people = array("Peter", "Joe", "Glenn", "Cleveland");

echo current($people) . "<br>";
echo next($people);
echo "<br/>";

//end

$people = array("Peter", "Joe", "Glenn", "Cleveland");

echo current($people) . "<br>";
echo end($people); 
echo "<br/>";

//pos

$people = array("Peter", "Joe", "Glenn", "Cleveland");

echo pos($people) . "<br>";
echo "<br/>";

//prev

$people = array("Peter", "Joe", "Glenn", "Cleveland");

echo current($people) . "<br>";
echo next($people) . "<br>";
echo prev($people);
echo "<br/>";
echo "<br/>";


//reset

$people = array("Peter", "Joe", "Glenn", "Cleveland");

echo current($people) . "<br>";
echo next($people) . "<br>";

echo reset($people);
echo "<br/>";

//sort

$cars=array("Volvo","BMW","Toyota");
sort($cars);

$clength=count($cars);
for($x=0;$x<$clength;$x++)
  {
  echo $cars[$x];
  echo "<br>";
  }
echo "<br/>";

//sizeof

$cars=array("Volvo","BMW","Toyota");
echo sizeof($cars);
echo "<br/>";

//shuffle

$my_array = array("red","green","blue","yellow","purple");

shuffle($my_array);
print_r($my_array);
echo "<br/>";

//push
$a=array("red","green");

echo "<pre>";
print_r($a);


array_push($a,"blue","yellow", "R kisu", "aro kisu", "dimu na");
print_r($a);
echo "<br/>";

//array_key_exists

$a = array("Volvo" => "XC90", "BMW" => "X5", "OK" => "");

echo "<pre>";
print_r($a);

if (array_key_exists('OK', $a) && !empty($a['OK'])) {
    echo $a['OK'];
} else {
    echo "Opps";
}

echo "<br/>";

//array_merge
$a = array("First_Name" => "gOOgLe", "Last_Name"=> "yAhOO", "Bing");
$a2=array("Personal"=> "0171717 23432", "Home"=> "0173743227");

$data = array_merge($a, $a2);

echo "<pre>";
print_r($data);

echo $data["First_Name"];

echo "<br/>";
echo $data["Personal"];

echo "<br/>";

//array_pad

$b = array("red", "green", "hobe ek'ta kisu", "aro kisu");


$data = array_pad($b, 5, "Jekono kisu");

echo "<pre>";
print_r($data);
echo "<br/>";

//pop

$a=array("red","green","blue");
echo "<pre>";
print_r($a);

array_pop($a);
print_r($a);



// problem 01 solution
for($i=5;$i>=1;$i--)
{
echo  str_repeat('*',$i);
echo "<br />";
}

echo "<hr />";

// problem 02 solution

echo '<p align="right">';
for($i=1;$i<=5;$i++)
{
echo  str_repeat('*',$i);
echo "<br />";
}
echo "</p>";

echo "<hr />";
// problem 03 solution

$j=1;

for($i=5;$i>=1;$i--)
{
echo  str_repeat("&nbsp;",$i-1);
echo  str_repeat('*',$j++);
echo "<br />";
}
