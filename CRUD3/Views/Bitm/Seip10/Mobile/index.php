<?php
include_once "../../../../Src/Bitm/Seip10/Mobile/Mobile.php";
?>
<a href="../../../../index.php">List of Project</a>
<a href="create.php">Add New Model</a>
<?php
$obj = new Mobile();
$Alldata = $obj->index();

if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>
<html>
<head>
    <title>Index | Data</title>
</head>
<body>
<table border="1">
    <tr>
        <th>SL</th>
        <th>Title</th>
        <th colspan="3">Action</th>
    </tr>
    <?php
    $serial = 1;
    if (isset($Alldata) && !empty($Alldata)) {

        foreach ($Alldata as $Singledata) {
            ?>

            <tr>
                <td><?php echo $serial++ ?></td>
                <td><?php echo $Singledata['title'] ?></td>
                <td><a href="show.php?id=<?php echo $Singledata['uniqre_id'] ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $Singledata['uniqre_id'] ?>">Edit</a></td>
                <td><a href="delete.php?id=<?php echo $Singledata['uniqre_id'] ?>">Delete</a></td>

            </tr>
        <?php }
    } else {
        ?>
        <tr>
            <td colspan="3">
                No available data
            </td>
        </tr>
    <?php } ?>
</table>
</body>
</html>
