<?php


include_once "../../../../vendor/autoload.php";

use Apps\Bitm\Seip131304\Mobile\Mobiles;
$obj=new Mobiles;

$obj->prepare($_GET);
$update=$obj->show();
//print_r($update);

if(isset($_SESSION['massage']) && !empty($_SESSION['massage'])){
    echo $_SESSION['massage'];
    unset($_SESSION['massage']);
}

?>

<html>
<head>
    <title>Mobile Models</title>
</head>
<body>
<a href="index.php">See List</a>
<fieldset>
    <legend>Update Models</legend>
    <form action="update.php" method="POST">
        <label for="">Update Mobiles</label>
        <input type="text" name="mobile_models" value="<?php echo $update['title']?>">
        <input type="hidden" name="id" value="<?php echo $update['unique_id']?>">
        <input type="submit" value="Update">
    </form>
</fieldset>
</body>
</html>