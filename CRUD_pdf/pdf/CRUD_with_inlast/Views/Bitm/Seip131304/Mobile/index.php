<?php
include_once "../../../../vendor/autoload.php";
use Apps\Bitm\Seip131304\Mobile\Mobiles;
$obj=new Mobiles;

$index=$obj->index();
//echo "<pre>";
//print_r($index) ;


if(isset($_SESSION['massage']) && !empty($_SESSION['massage'])){
    echo $_SESSION['massage'];
    unset($_SESSION['massage']);
}

?>

<a href="create.php">Add New Mobile</a>

<!doctype html>
<html lang="en">
<head>
    
    <title>List of Mobile Models</title>
</head>
<body>
    <table border="1">
        <tr>
            <th>SL</th>
            <th>Models</th>
            <th colspan="3">Action</th>
        </tr>
        <?php
        $sl=1;
        if (isset($index)&& !empty($index)){
            foreach ($index as $allitem){?>
                <tr>
                    <td><?php echo $sl++;//echo $allitem['id']?></td>
                    <td><?php echo $allitem['title']?></td>
                    <td><a href="view.php?id=<?php echo $allitem['unique_id']?>">View</a></td>
                    <td><a href="edit.php?id=<?php echo $allitem['unique_id']?>">Update</a></td>
                    <td><a href="delete.php?id=<?php echo $allitem['unique_id']?>">Delete</a></td>
                </tr>
           <?php }
        }else{
        ?>

        <tr>
            <td  colspan="3">No avaible data</td>
        </tr>
        <?php } ?>
    </table>
    
    
    <h1>List of Mobiles</h1>
    <div>
        <?php //echo substr($paging,0,strlen($paging)-2)?>
    </div>
<!--    <a href="trashed.php">All Trashed Data</a>-->
    
    <div><span>Search / Filter</span>
        <span id="utility">Download as <a href="pdf.php">PDF</a> |<span id="utility">Download as <a href="xl.php">Excel</a> | <span id="utility"><a href="phpmailer.php">Mail to Friend</a> |<a href="create.php">Create New</a></span>
        <form action="index.php">
            <select name="itemPerPage" id="">
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
                <option value="40">40</option>
                <option value="50">60</option>
            </select>
            <button type="submit">Go</button>
        </form>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</body>
</html>