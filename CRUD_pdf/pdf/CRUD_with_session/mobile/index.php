<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 7/16/2016
 * Time: 9:56 PM
 */

session_start();
?>

<a href="create.php">Add New Mobile</a>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>List of Mobile Models</title>
</head>
<body>
<table border="1">
    <tr>
        <th>SL</th>
        <th>Mobile</th>
        <th>Laptop</th>
        <th colspan="4">Action</th>
    </tr>
    <?php
    $sl=1;
    if (isset($_SESSION['alldata'])&& !empty($_SESSION['alldata'])){
        foreach ($_SESSION['alldata'] as $key=> $allitem){?>
            <tr>
                <td><?php echo $sl++;//echo $allitem['id']?></td>
                <td><?php  if(isset($allitem['mobile_models'])){echo $allitem['mobile_models'];}?></td>
                <td><?php if(isset($allitem['laptop_models'])){echo $allitem['laptop_models'];}?></td>
                <td><a href="show.php?id=<?php echo $key ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $key ?>">Update</a></td>
                <td><a href="delete.php?id=<?php echo $key ?>">Delete</a></td>
            </tr>
        <?php }
    }else{
        ?>

        <tr>
            <td  colspan="3">No avaible data</td>
        </tr>
    <?php } ?>
</table>
</body>
</html>
