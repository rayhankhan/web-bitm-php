<?php
//include_once "../vendor/autoload.php";
//
//use App\Users\Users;
//$obj = new Users();
//$obj->prepare($_GET);
//
//$onedata = $obj->show();
//echo "<pre>";
//print_r($_SESSION['Login_data']);
//echo "</pre>";


//if (isset($_SESSION['Login_data']) && !empty($_SESSION['Login_data'])) {
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Data Update Form</title>
        <!-- CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">

        <!-- JS -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
    <!--contact-form-->
    
    <div class="contact-form">

        <div class="container">

            <div class="row">
                <div class="col-md-5 col-md-offset-3">
                    <h3>Profile Update Form</h3>
                    <strong class="text-success"> </strong>

                    <form action="profile-process.php" method="post" enctype="multipart/form-data">

                        <label>Full Name</label>
                        <input class="form-control" type="text" name="full_name" value="">
                        <p class="text-danger"></p><br>

                        <label>Father Name</label>
                        <input class="form-control" type="text" name="father_name" value="">
                        <p class="text-danger"></p><br>

                        <label>Mother Name</label>
                        <input class="form-control" type="text" name="mother_name" value="">
                        <p class="text-danger"></p><br>

                        <label>Update your gender: </label>
                        <label for="male">
                            <input type="radio" name="gender" value="Male"  id="male">
                            Male
                        </label>
                        <label for="female">
                            <input type="radio" name="gender" value="Female"  id="female">
                            Female
                        </label>
                        <p class="text-danger"></p><br><br>

                        <label>Phone: </label>
                        <input type="number" name="phone" class="form-control" >
                        <p class="text-danger"></p><br>

                        <label>FAX Number</label>
                        <input type="number" name="fax_number" class="form-control" >
                        <p class="text-danger"></p><br>

                        <label>Web Address</label>
                        <input type="url" name="web_address" class="form-control"
                               value="">
                        <p class="text-danger"></p><br>

                        <label>Date of Birth: </label>
                        <input type="date" name="dateofbirth" class="form-control" value="">
                        <p class="text-danger"> </p><br>

                        <label>Height</label>
                        <input class="form-control" type="text" name="height" value="">
                        <p class="text-danger"> </p><br>

                        <label>Occupation</label>
                        <input class="form-control" type="text" name="occupation" value="">
                        <p class="text-danger"></p><br>

                        <label>Education Status</label>
                        <input class="form-control" type="text" name="education_status" value="">
                        <p class="text-danger"></p><br>

                        <label>Religion</label>
                        <input class="form-control" type="text" name="religion" value="">
                        <p class="text-danger"> </p><br>

                        <label>Marital Status</label>
                        <input class="form-control" type="text" name="marital_status" value="">
                        <p class="text-danger"></p><br>

                        <label>Current Job Status</label>
                        <input class="form-control" type="text" name="current_job" value="">
                        <p class="text-danger"><?php $obj->Validation("current_job"); ?></p><br>

                        <label>Nationality</label>
                        <input class="form-control" type="text" name="nationality" value="">
                        <p class="text-danger"></p><br>

                        <label>Interested: </label>
                        <label>
                            <input type="checkbox" name="interested[]" value="Reading" Reading </label>
                        <label>
                            <input type="checkbox" name="interested[]"value="Traveling" Traveling </label>
                        <label>
                            <input type="checkbox" name="interested[]" value="Blogging"  Blogging </label>
                        <label>
                            <input type="checkbox" name="interested[]" value="Collecting"  Collecting </label>
                        <p class="text-danger"> </p><br><br>

                        <label>Bio</label>
                        <textarea class="form-control" name="bio" rows="5"></textarea>
                        <p class="text-danger"></p><br>

                        <label>NID</label>
                        <input type="number" name="nid" class="form-control" value=">
                        <p class="text-danger"></p><br>

                        <label>passport Number</label>
                        <input type="number" name="passport_number" class="form-control"
                               value="<?php //echo $singledata['phone_number']?>">
                        <p class="text-danger"><?php $obj->Validation("passport_number"); ?></p><br>

                        <label>Skills Area: </label><br>
                        <label>
                            <input type="checkbox" name="skills_area[]" value="Accuracy"  Accuracy </label>
                        <label>
                            <input type="checkbox" name="skills_area[]"
                                   value="Budgeting" Budgeting </label>
                        <label>
                            <input type="checkbox" name="skills_area[]"
                                   value="Calculating_data" Calculating data </label>
                        <label>
                            <input type="checkbox" name="skills_area[]"
                                   value="Defining_problems"  Defining problems </label>
                        <label>
                            <input type="checkbox" name="skills_area[]"
                                   value="Editing"  Editing </label>
                        <label>
                            <input type="checkbox" name="skills_area[]"
                                   value="Initiator"  Initiator </label>
                        <p class="text-danger"></p><br><br>

                        <label>Language Area: </label><br>
                        <label>
                            <input type="checkbox" name="language_area[]"
                                   value="English"  English </label>
                        <label>
                            <input type="checkbox" name="language_area[]"
                                   value="French"  </label>
                        <label>
                            <input type="checkbox" name="language_area[]"
                                   value="German"  </label>
                        <label>
                            <input type="checkbox" name="language_area[]"
                                   value="Japanese"  Japanese </label>
                        <label>
                            <input type="checkbox" name="language_area[]"
                                   value="Bangle"  </label>
                        <p class="text-danger"></p><br><br>

                        <label>Blood Group: </label>
                        <select name="Blood_group" class="form-control">
                            <option selected disabled hidden>Select Here</option>
                            <option
                                value="A+" 
                        </option>
                            <option value="O+" >
                                O+
                            </option>
                            <option
                                value="B+" >
                                B+
                            </option>
                            <option
                                value="AB+" >
                                AB+
                            </option>
                            <option
                                value="A-" >
                                A-
                            </option>
                            <option
                                value="O-" >
                                O-
                            </option>
                            <option
                                value="B-" >
                                B-
                            </option>
                            <option
                                value="AB-" >
                                AB-
                            </option>
                        </select>
                        <p class="text-danger"></p><br><br>


                        <label>Address</label>
                        <input class="form-control" type="text" name="address_one" value="" placeholder="Address one">
                        <p class="text-danger"></p><br>

                        <input class="form-control" type="text" name="address_two" value=""
                               placeholder="Address two"><br>

                        <input class="form-control" type="number" name="post" value="" placeholder="Post code">
                        <p class="text-danger"></p><br>

                        <input class="form-control" type="text" name="state" value="" placeholder="State">
                        <p class="text-danger"></p><br>

                        <input class="form-control" type="text" name="city" value="" placeholder="City">
                        <p class="text-danger"></p><br>

                        <label>Others</label>
                        <input class="form-control" type="text" name="others"><br>

                        <label>Profile Pic</label>
                        <input type="file" name="photo">
                        
                        <p class="text-danger"></p><br>
                        <p class="text-danger"></p><br>


                        <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
                        <input class="btn btn-block" type="submit" value="Update" name="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--/contact-form-->
    </body>
    </html>
    


