<?php

//frist examp boolval()

echo '0: '.(boolval(0)) ? 'true':'false'."\n";
echo '42: '.(boolval(42)) ? 'true':'false'."\n";
echo '0.0: '.(boolval(0.0)) ? 'true':'false'."\n";
echo '4.2: '.(boolval(4.2)) ? 'true':'false'."\n";
echo '"": '.(boolval("")) ? 'true':'false'."\n";
echo '"string": '.(boolval("string")) ? 'true':'false'."\n";
echo '"0": '.(boolval(0)) ? 'true':'false'."\n";
echo '"1": '.(boolval("1")) ? 'true':'false'."\n";
echo '[1,2]: '.(boolval([1,2])) ? 'true':'false'."\n";
echo '[]: '.(boolval([])) ? 'true':'false'."\n";
echo 'stdClass: '.(boolval(new stdClass)) ? 'true':'false'."\n";


//Second exam empty()

$var=0;


if(empty($var)){
	echo '$var is either 0,empty, or not set at all';
}

if(isset($var)){
	echo '$var is set even though it is empty';
}



//third exam gettype()

$data = array(1, 1., NULL, new stdClass, 'foo');

foreach ($data as $value) {
    echo gettype($value), "\n";
}

//forth exam is_array

$yes = array('this', 'is', 'an array');

	echo is_array($yes) ? 'Array' : 'not an Array';
	echo "\n";

$no = 'this is a string';

	echo is_array($no) ? 'Array' : 'not an Array';

	
	
	
//fifth exam is_int()

$values = array(23, "23", 23.5, "23.5", null, true, false);
	foreach ($values as $value) {
		echo "is_int(";
		var_export($value);
		echo ") = ";
		var_dump(is_int($value));
}



//sixth examp

	

		
//seventh examp is_null() 


error_reporting(E_ALL);

$foo = NULL;
var_dump(is_null($inexistent), is_null($foo));


//eight examp is_object()

function get_students($obj){
	
    if (!is_object($obj)) {
        return false;
    }

    return $obj->students;
}

// Declare a new class instance and fill up 
// some values
	$obj = new stdClass();
	$obj->students = array('Kalle', 'Ross', 'Felipe');

	var_dump(get_students(null));
	var_dump(get_students($obj));


//ninth examp is_string()

$values = array(false, true, null, 'abc', '23', 23, '23.5', 23.5, '', ' ', '0', 0);
	foreach ($values as $value) {
		echo "is_string(";
		var_export($value);
		echo ") = ";
		echo var_dump(is_string($value));
}

 

//tenth examp is isset()

$var = '';
if (isset($var)) {
    echo "This var is set so I will print.";
}
	$a = "test";
	$b = "anothertest";

	var_dump(isset($a));      
	var_dump(isset($a, $b)); 

	unset ($a);

	var_dump(isset($a));     
	var_dump(isset($a, $b)); 

	$foo = NULL;
	var_dump(isset($foo));

//eleventh examp print_r()

$a = array ('a' => 'apple', 'b' => 'banana', 'c' => array ('x', 'y', 'z'));
print_r ($a);

//twelveth examp serialize()

$conn = odbc_connect("webdb", "php", "chicken");
$stmt = odbc_prepare($conn,"UPDATE sessions SET data = ? WHERE id = ?");
$sqldata = array (serialize($session_data), $_SERVER['PHP_AUTH_USER']);

if (!odbc_execute($stmt, $sqldata)) {
    $stmt = odbc_prepare($conn,
     "INSERT INTO sessions (id, data) VALUES(?, ?)");
    if (!odbc_execute($stmt, $sqldata)) {
       }
}


//forteenth examp //thirteenth examp unserialize()


$conn = odbc_connect("webdb", "php", "chicken");
$stmt = odbc_prepare($conn, "SELECT data FROM sessions WHERE id = ?");
$sqldata = array($_SERVER['PHP_AUTH_USER']);

if (!odbc_execute($stmt, $sqldata) || !odbc_fetch_into($stmt, $tmp)) {
    
    $session_data = array();
} else {
    
    $session_data = unserialize($tmp[0]);
    if (!is_array($session_data)) {
        
        $session_data = array();
    }
}

//forteenth examp unset()

function destroy_foo() 
{
    global $foo;
    unset($foo);
}

	$foo = 'bar';
	destroy_foo();
echo $foo;



//fifteenth examp var_dump()

$a = array(1, 2, array("a", "b", "c"));
var_dump($a);
	
?>
