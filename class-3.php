<?php
echo "Hello world";

// ... more code

echo "Last statement";


 echo 'if you want to serve PHP code in XHTML or XML documents,
                use these tags'; ?>

You can use the short echo tag to <?= 'print this string' ?>.
    It's always enabled in PHP 5.4.0 and later, and is equivalent to
    <?php echo 'print this string' ?>.

<? echo 'this code is within short tags, but will only work '.
            'if short_open_tag is enabled'; ?>

<script language="php">
        echo 'some editors (like FrontPage) don\'t
              like processing instructions within these tags';
    </script>
?>
