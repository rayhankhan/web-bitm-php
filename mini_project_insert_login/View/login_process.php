<?php

include_once "../vendor/autoload.php";

use App\Users\Users;

$obj = new Users();
$obj->prepare($_POST);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!empty($_POST['username'])) {
        if (!empty($_POST['password'])) {
            $obj->prepare($_POST)->login();
        } else {
            $_SESSION['Message'] = "Enter password";
            header('location:login.php');
        }
    } else {
        $_SESSION['Message'] = "Enter username";
        header('location:login.php');
    }
} else {
    $_SESSION['Message'] = "Opps something going wrong!";
    header('location:login.php');
}