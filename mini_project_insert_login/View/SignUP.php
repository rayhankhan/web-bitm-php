<?php
?>
<!DOCTYPE html>
<html>
        
    <head>
        <title> Signup Page</title>
        <link rel="stylesheet" type="text/css" href="Style.css">
    </head>
    
    <body>
           <div class="container">
    <div class="col-md-6">
    <div id="logbox">
      <form  method="post" action="signup_process.php">
        <h1>create an account</h1>
        
        <input name="username" type="text" placeholder="What's your username?"  required="required" class="input pass"/>
        
        <input name="password" type="password" placeholder="Choose a password" required="required" class="input pass"/>
        
        <input name="cpassword" type="password" placeholder="Confirm password" required="required" class="input pass"/>
        
        <input name="email" type="email" placeholder="Email address" class="input pass"/>
        
        <input type="submit" value="Sign me up!" class="inputButton"/>
        <input type="reset" name="Reset me" value="reset me" class="resetButton"/>
        
        <div class="text-center">
            
            already have an account? <a href="login.php" id="login_id">login</a>
            
        </div>
      </form>
    </div>
   </div>    
  </div>

    </body>
</html>