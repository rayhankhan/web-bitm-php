<?php
session_start();
if (isset($_SESSION['Msg']) && !empty($_SESSION['Msg'])) {
    echo "<h1>".$_SESSION['Msg']."</h1>";
    unset($_SESSION['Msg']);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>login page</title>
	<link rel="stylesheet" type="text/css" href="Style.css">
</head>
<body>
<div class="col-md-9">
    <div id="logbox">
      <form method="post" action="login_process.php">
        <h1>account login</h1>
        <input name="username" type="username" placeholder="enter your username" class="input pass"/>
        <input name="password" type="password" placeholder="enter your password" required="required" class="input pass"/>
        <input type="submit" value="Log me in!" class="inputButton"/>
        <div class="text-center">
            <a href="SignUP.php">create an account</a> - <a href="#">forgot password</a>
                </div>
      </form>
      <?php if (isset($_SESSION['Message'])) { ?>
                    <div class="alert alert-success">
                        <strong><span class=""></span>
                            <?php
                            echo $_SESSION['Message'];
                            unset($_SESSION['Message']);
                            ?>
                        </strong>
                    </div>
                <?php } ?>
    </div>
</div>

</body>
</html>


